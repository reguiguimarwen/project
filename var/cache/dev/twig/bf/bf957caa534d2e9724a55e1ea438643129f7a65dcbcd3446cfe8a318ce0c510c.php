<?php

/* base.html.twig */
class __TwigTemplate_13eeb902aac80672081b669a5de58a58ce816def9e6298eee8f3578715665c50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f1d85e28deb8889004925253ac792338fea5a28f35fe4fb81b14a27a64e017b = $this->env->getExtension("native_profiler");
        $__internal_4f1d85e28deb8889004925253ac792338fea5a28f35fe4fb81b14a27a64e017b->enter($__internal_4f1d85e28deb8889004925253ac792338fea5a28f35fe4fb81b14a27a64e017b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_4f1d85e28deb8889004925253ac792338fea5a28f35fe4fb81b14a27a64e017b->leave($__internal_4f1d85e28deb8889004925253ac792338fea5a28f35fe4fb81b14a27a64e017b_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_ac94ec0be7d09a5a373c97a6e43f20e75f67d70d375440df897afbc51868d531 = $this->env->getExtension("native_profiler");
        $__internal_ac94ec0be7d09a5a373c97a6e43f20e75f67d70d375440df897afbc51868d531->enter($__internal_ac94ec0be7d09a5a373c97a6e43f20e75f67d70d375440df897afbc51868d531_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_ac94ec0be7d09a5a373c97a6e43f20e75f67d70d375440df897afbc51868d531->leave($__internal_ac94ec0be7d09a5a373c97a6e43f20e75f67d70d375440df897afbc51868d531_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_64fa4f8a9c70512759ee12f753c54eb1d8d6e20a98f3339716e43a14244edbfd = $this->env->getExtension("native_profiler");
        $__internal_64fa4f8a9c70512759ee12f753c54eb1d8d6e20a98f3339716e43a14244edbfd->enter($__internal_64fa4f8a9c70512759ee12f753c54eb1d8d6e20a98f3339716e43a14244edbfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_64fa4f8a9c70512759ee12f753c54eb1d8d6e20a98f3339716e43a14244edbfd->leave($__internal_64fa4f8a9c70512759ee12f753c54eb1d8d6e20a98f3339716e43a14244edbfd_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_38e38025865e635ccce1cb465feea2f01b9cc167569dca557dfaca763aebc55b = $this->env->getExtension("native_profiler");
        $__internal_38e38025865e635ccce1cb465feea2f01b9cc167569dca557dfaca763aebc55b->enter($__internal_38e38025865e635ccce1cb465feea2f01b9cc167569dca557dfaca763aebc55b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_38e38025865e635ccce1cb465feea2f01b9cc167569dca557dfaca763aebc55b->leave($__internal_38e38025865e635ccce1cb465feea2f01b9cc167569dca557dfaca763aebc55b_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1b6957ad53287c707334804596794225ee149dd810f1a7441c8a1c14754e66e9 = $this->env->getExtension("native_profiler");
        $__internal_1b6957ad53287c707334804596794225ee149dd810f1a7441c8a1c14754e66e9->enter($__internal_1b6957ad53287c707334804596794225ee149dd810f1a7441c8a1c14754e66e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_1b6957ad53287c707334804596794225ee149dd810f1a7441c8a1c14754e66e9->leave($__internal_1b6957ad53287c707334804596794225ee149dd810f1a7441c8a1c14754e66e9_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
