<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_ed5b23509ba6588b8772bbe53cd6952f9048fce2cf5db3ae719c598d15230898 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_952d63be3ee94fb8f473fed326c746dee658ec75e6801946d6046d2a0bda962f = $this->env->getExtension("native_profiler");
        $__internal_952d63be3ee94fb8f473fed326c746dee658ec75e6801946d6046d2a0bda962f->enter($__internal_952d63be3ee94fb8f473fed326c746dee658ec75e6801946d6046d2a0bda962f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_952d63be3ee94fb8f473fed326c746dee658ec75e6801946d6046d2a0bda962f->leave($__internal_952d63be3ee94fb8f473fed326c746dee658ec75e6801946d6046d2a0bda962f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_551b88774193fd3e32bd23966cb288ea0fcb06d5a05821c3e186339a54d6253b = $this->env->getExtension("native_profiler");
        $__internal_551b88774193fd3e32bd23966cb288ea0fcb06d5a05821c3e186339a54d6253b->enter($__internal_551b88774193fd3e32bd23966cb288ea0fcb06d5a05821c3e186339a54d6253b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_551b88774193fd3e32bd23966cb288ea0fcb06d5a05821c3e186339a54d6253b->leave($__internal_551b88774193fd3e32bd23966cb288ea0fcb06d5a05821c3e186339a54d6253b_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_834a18f42b4ec335e62c5b11e9493b9094112ca669f4e37e500440ff4c6e3f3d = $this->env->getExtension("native_profiler");
        $__internal_834a18f42b4ec335e62c5b11e9493b9094112ca669f4e37e500440ff4c6e3f3d->enter($__internal_834a18f42b4ec335e62c5b11e9493b9094112ca669f4e37e500440ff4c6e3f3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_834a18f42b4ec335e62c5b11e9493b9094112ca669f4e37e500440ff4c6e3f3d->leave($__internal_834a18f42b4ec335e62c5b11e9493b9094112ca669f4e37e500440ff4c6e3f3d_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_b263564db1e3bac3d734b6424604d57707a0b91c857497695b9cd6a55c00ace9 = $this->env->getExtension("native_profiler");
        $__internal_b263564db1e3bac3d734b6424604d57707a0b91c857497695b9cd6a55c00ace9->enter($__internal_b263564db1e3bac3d734b6424604d57707a0b91c857497695b9cd6a55c00ace9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_b263564db1e3bac3d734b6424604d57707a0b91c857497695b9cd6a55c00ace9->leave($__internal_b263564db1e3bac3d734b6424604d57707a0b91c857497695b9cd6a55c00ace9_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
