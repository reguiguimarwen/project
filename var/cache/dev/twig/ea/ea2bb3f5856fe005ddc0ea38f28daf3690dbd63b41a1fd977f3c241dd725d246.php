<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0a1c80e9cbbe97576dc70e74e198009d2e92c700a23dcd3b8de77362488dfb53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed330bcf2c37c998bdd4a62c79365bdb874dfeb65e9f098025b1b2e1ee95b009 = $this->env->getExtension("native_profiler");
        $__internal_ed330bcf2c37c998bdd4a62c79365bdb874dfeb65e9f098025b1b2e1ee95b009->enter($__internal_ed330bcf2c37c998bdd4a62c79365bdb874dfeb65e9f098025b1b2e1ee95b009_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed330bcf2c37c998bdd4a62c79365bdb874dfeb65e9f098025b1b2e1ee95b009->leave($__internal_ed330bcf2c37c998bdd4a62c79365bdb874dfeb65e9f098025b1b2e1ee95b009_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_0f0d32ddbdda2ebeb4c39fa115a36079e363e30056119e00c4974fb91d639559 = $this->env->getExtension("native_profiler");
        $__internal_0f0d32ddbdda2ebeb4c39fa115a36079e363e30056119e00c4974fb91d639559->enter($__internal_0f0d32ddbdda2ebeb4c39fa115a36079e363e30056119e00c4974fb91d639559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_0f0d32ddbdda2ebeb4c39fa115a36079e363e30056119e00c4974fb91d639559->leave($__internal_0f0d32ddbdda2ebeb4c39fa115a36079e363e30056119e00c4974fb91d639559_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a71034240cbc054a09ce2a367b77a3c5a22cda507d8230a044a6a08a406e7df9 = $this->env->getExtension("native_profiler");
        $__internal_a71034240cbc054a09ce2a367b77a3c5a22cda507d8230a044a6a08a406e7df9->enter($__internal_a71034240cbc054a09ce2a367b77a3c5a22cda507d8230a044a6a08a406e7df9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_a71034240cbc054a09ce2a367b77a3c5a22cda507d8230a044a6a08a406e7df9->leave($__internal_a71034240cbc054a09ce2a367b77a3c5a22cda507d8230a044a6a08a406e7df9_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_4ba6eb1e113244e159789239cda1bb040c428831f0c01b8f37062b3516845578 = $this->env->getExtension("native_profiler");
        $__internal_4ba6eb1e113244e159789239cda1bb040c428831f0c01b8f37062b3516845578->enter($__internal_4ba6eb1e113244e159789239cda1bb040c428831f0c01b8f37062b3516845578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_4ba6eb1e113244e159789239cda1bb040c428831f0c01b8f37062b3516845578->leave($__internal_4ba6eb1e113244e159789239cda1bb040c428831f0c01b8f37062b3516845578_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
